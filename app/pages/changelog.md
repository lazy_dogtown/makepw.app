

<hr>
<a name="changelog"></a>
<div class="row" align="left">
<div  class="col-sm-2">
<img src="/static/barking-dog-med.png">
</div>
<div class="col-sm-10" >
    <br>
    <h2>Changelog & Roadmap</h2>
</div>
</div>


<hr>

<div class="row" markdown="1" align="left">
<div  class="col-sm-2">

</div>
<div class="col-sm-10" markdown="1">

#### Todo

- pure python, also for makepw-web -> mpw-ctl
- xkcd-mode
- languages for interface [en|fr|es|de]
- API xml
- Secure Passwords - musings
- for api_ create checksum() for the text-output to verify

----------------------------------------

#### v0.x - Changelog

- v0.8.10 - 2015-05-05

    - transition to run w/out tweaks on *bsd
    - went from easy_install to pip install

- v0.8 - 2013-09-02 
    - new pw-gen code, see http://security.stackexchange.com/questions/41501/is-the-following-password-generator-secure
    - app_config now in a separate config.file that gets generated on first run (creating a random SECRET)
    - installation + configuration - manuals 
    - sourcecode - release 
    - manage.py
    - very simple api (json,plain,yaml) + docs
    - customizable site_name, docs & some appearance-options
    - custom.links  
    - privacy-statement & terms of service
    - official website runs now with https only 
    - changed makepw.com to makepw.app @ bitbucket
    

-----------------------

    
- v0.7 - 2013-08-27
    - released @ makepw.com
    - some docs

-----------------------

- v0.6 - 2013-06-23
    - polish and customer-usable

-----------------------


- v0.5 - 2012-12-17
    - added bootstrap - ui
    - select-options via navigation

-----------------------


- v0.4 - 2012-12-10 
    - added /<length>/ - option
    - changed to geany in a bottle

-----------------------

    
- v0.3 - 2012-10-10
    - release for interal purposes
    - plain text-page with fixed pw-length 

-----------------------


</div>
</div>
