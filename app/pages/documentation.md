

<div class="row">
<div  class="col-sm-2">
<img src="/static/barking-dog-med.png">
</div>
<div class="col-sm-10" >
    <br>
    <h4>MakePW.com - Online Password-Generator</h4>
</div>
</div>

<div id="acco2" markdown="1">
<h7> Table of Contents </h7>
<div markdown="1">


[TOC]



</div>
</div>


-----------------

[MakePW.com](http://makepw.com/) is a service that generates secure, random passwords, 24/7.
PW-default-length is 16 characters and can be adjusted 
from 8 - 128 characters. There's also an option to get the 
password-list as plain ascii-text file or access an RESTful 
[API](/doc/#api) that creates JSON/YAML or Plaintext-output.


This webapp works with javascript disabled, except for 
navigation on smartphones; you
still can adjust the value for password-length manually and
choose between 8 and 128 characters the following way: 
[makepw.com/32/](http://makepw.com/32/)

The sourcecode of this [python/flask](http://flask.pocoo.org/) - based 
WebApp is [released as open-source-software](https://bitbucket.org/lazy_dogtown/makepw.app/)  
to be used for individuals or within organizational infrastructure; please refer to the [installation](#install) - section
below to find needed manuals to setup and configure your own version. 

This WebApp is Free Software [(free as in free speech)](http://www.gnu.org/philosophy/free-sw.html) and
licensed under the Terms of the [GNU Affero General Public License](http://www.gnu.org/licenses/agpl.html)

Our Privacy-Statement and Terms of Service are available [here](/info/#privacy).

<a name="usage"></a>

<hr>

#### Usage:

- [makepw.com/](https://makepw.com/) - generate passwords with 16 characters (default)
- [makepw.com/32/](https://makepw.com/32/) - generate passwords with 32  characters 
- [makepw.com/plain/](https://makepw.com/plain/) - generate passwords with 16  chars and export as ascii-text
- [makepw.com/plain/32](https://makepw.com/plain/32/) - generate passwords with 32  chars and export as ascii-text


- [makepw.com/about/#usage](http://makepw.com/about/#usage) - this documentation




<a name="tools"></a>


-----------------

#### Password-Tresors and Tools

For safely storing passwords we suggest using an offline password-tresor like the following, 
that are available for a wide range of operating systems like Linux, OSX and Windows. Please
** avoid using an online-password-store.** Really. 

- [KeePass](http://keepass.info/) 
- [KeePassX](http://www.keepassx.org/)
- [LastPass](https://lastpass.com)

**Linux-Users**

- use `pwgen 32` or `makepasswd --chars 32` to generate secure passwords 



<a name="install"></a>


-----------------

#### Docs: Installation & Configuration

you can download and run this web-application and tweak for your own needs. 

- [Download](https://bitbucket.org/lazy_dogtown/makepw.app) sourcecode from bitbucket.org
- [Installation - Manual](/doc/#install), to be found in the sourcecode under pages/installation.md
- [Configuration - Manual](/doc/#config), to be found in the sourcecode under pages/configuration.md
- [API - Manual](/doc/#api), to be found in the sourcecode under pages/api.md
- [ChangeLog](/doc/#changelog), to be found in the sourcecode under pages/changelog.md
- [License](/info/#license), to be found in the sourcecode under License.txt



<a name="code"></a>


-----------------

#### PW-generation-code

- credits: Brendan Long / see [this discussion](http://security.stackexchange.com/questions/41501/is-the-following-password-generator-secure)


~~~.python

def makepw(l=32):

    from random import SystemRandom
    
    rng = SystemRandom()
    
    allchars = string.ascii_letters + string.digits    
    
    try:
        passwordLength = int(l)
    except:
        passwordLength = 32

    # not less 
    if passwordLength < 8:
        passwordLength = 32
    elif passwordLength > 128:
        passwordLength = 32

    pw = "".join([rng.choice(allchars) for i in range(passwordLength)])
    return(pw)


~~~

<a name="privacy"></a>


-----------------

#### Privacy-Policy & TOS


** Disclaimer: We cannot and will not guarantee the confidentiality of the
passwords generated via [makepw.com](https://makepw.com/). 
If you operate in TinFoilHat - mode or use a heavily secured/firewalled
or proxied environment we strongly advise you NOT TO USE THIS SERVICE**,  but
use some of the [tools](#tools) instead to generate passwords locally.


We do not store any generated passwords or any user-based information;
not even ips that accessed this service. What we do store is any 
unusual accesses and attempts to misuse this service, namely
hacking'n'stuff. 

To exclude all possibilites of unwanted 3rd-party-ugliness we advise you
to use plugins like AdBlock, Ghostery, RequestPolicy and NoScript. These
should be available as plugins for modern browsers.  

There are no 3rd-parties - trackers. cookies or sources included. Social-Buttons
are planned but will reside on an own page and will be disabled by default. 
Google-Adsense might occure in the future but can be avoided using mentioned 
browser-plugins. 

We do not use cookies (yet), but might in the future to store session-ids
and user-based settings. 

**TOS**

You can use this service or software free of charge. We cannot and will not 
guarantee the confidentiality of the passwords generated. If you need 
confidential passwords you should use other tools like KeePass/LastPass
or `pwgen` on Linux, to genreate your passwords localy. 

<a name="contact"></a>

-----------------

#### Contact

you may contact us via email: info@makepw.com or [bitbucket](https://bitbucket.org/lazy_dogtown/makepw.app);
feel free to create an issue if you encounter a bug or other uncorrectness. 


<a name="license"></a>

-----------------

#### Copyright & License 

(c) copyright 2012,2013 makepw.com
 
this software is free software and licensed unter the 
terms of the GNU Affero General Public License; 
see License.txt for more details

