
<hr>
<a name="config"></a>
<div class="row" align="left">
<div  class="col-sm-2">
<img src="/static/barking-dog-med.png">
</div>
<div class="col-sm-10" >
    <br>
    <h2>Configuration-Manual</h2>
</div>
</div>


<hr>

<div class="row" markdown="1" align="left">
<div  class="col-sm-2">

</div>
<div class="col-sm-10" markdown="1">


### config.py 

this is the global config_file where you might adjust some variables
in appearance and behavior. all config_options MUST be in UPPER_LETTERS,
and python-notion (e.g. strings escaped like: VAR = "string", numbers as numbers 
like INT = 42) to be readable by the application. 

- SITE_NAME: the string that appers on the top-left 
- DEBUG: shouldnt be used, except for development
- SECRET_KEY: a secure random string is generated each time you run -init; this feature is
  not yet used, but might be later if using cookies/sessions
- ABOUT_NAME: name for the link that appears on the top right location and below
  the PW-Field 
  

**sample-config:**

~~~

SITE_NAME="MakePW.com"
DEBUG = False
SECRET_KEY = "this_feature_is_not_yet_used"
ABOUT_NAME="Info"



~~~

Hidden Feature (please dont use this one)

- PRIVATE: should be set to "yes"; this var is used on the official MakePW.com to have the
  social-page with facebook/g+/twitter/reddit - buttonz displayed; if you want something
  like this, create a social.md in pages/ and check the templates/social.html 


### Changing Appearance & Navigation

#### Name, Headers & Logo

- Site-name and the title of the About/Info - Link (on the top right) can be changed by editing
  config.py 
- Site-Logo (left, in the header) is located in app/static/site-icon.png
- Favicon can be found in app/static/favicon.ico


#### customize Subnav-Links

- to change the link-list on the right hand (see Docs/Get_your_version [online](http://makepw.com/) ) just
  edit a file called "custom.links" in the app/ - folder with the following format (default-entries)
  
~~~
    home      :: /
    google    :: https://www.google.com/
~~~ 

#### customized documentation 

- all texts and docs are written using [Markdown](http://daringfireball.net/projects/markdown/syntax)

- you can change the document, presented under the [/info/](/info/) link, by editing app/pages/documentation.md
  for custom user-documentation; this file is excluded in .gitignore

- you can change the text below the password-field (**About** on the online-version) with editing app/pages/about.md; 
  the title of the Link will change with the config_option: ABOUT_NAME [see config.py](#config-py)

### changing listening ip/port

the IP/Port this app listens is configured in makepw-web, a shell-script to
manage your running application. 

defaults (you might change that to fit your requirements):

~~~

host = "0.0.0.0"
port = "9876"

~~~

these variables get exported as SHELL_VARS and read by flask.app or transfered
to gunicorn_bind_vars

ftr: there is a possibility to manage this better, 
[see this stackoverflow - thread](http://stackoverflow.com/questions/14566570/how-to-use-flask-script-and-gunicorn)
but i'm to lazy atm :)


### nginx - config

a short nginx-config below; adjust https-config accordingly **(ssl is strongly advised!)**

~~~
server {

  listen       80;
  server_name  pw.example.com;
 
  access_log  /var/log/nginx/makepw.access.log;
  error_log   /var/log/nginx/makepw.error.log;

  gzip on;

  location /static/ {
  
       root /htdocs/makepw.app/app;
       expires 1d;

    }


  location / {
       expires off;
       proxy_set_header Host $host;
       proxy_pass http://127.0.0.1:9876;

    }

}





~~~


</div>
</div>
