

<a name="install"></a>
<div class="row" align="left">
<div  class="col-sm-2">
<img src="/static/barking-dog-med.png">
</div>
<div class="col-sm-10" >
    <br>
    <h2>Installation-Manual</h2>
</div>
</div>


<hr>

<div class="row" markdown="1" align="left">
<div  class="col-sm-2">

</div>
<div class="col-sm-10" markdown="1">

### Requirements

- Python 2.6 or any later 2.x 
- python-virtualenv
- git 
- bash (for managing the application)
- build-environment is usually not needed (gcc/make; `aptitude install build-essential` under debian, `yum groupinstall "Development Tools"` for centos)
- tested on linux (debian, centos)

#### Debian


~~~

aptitude install python-virtualenv git

~~~

#### CentOS

~~~

yum install python-setuptools git
umask 022
easy_install virtualenv

~~~

#### FreeBSD

~~~
pkg install py27-virtualenv git
~~~

### Sourcecode & Installaion


- download sources from bitbucket

~~~

git clone https://bitbucket.org/lazy_dogtown/makepw.app.git

~~~

- running `./makepw-web -i` (-init); this will locally install all needed 
libs and dependencies, using python-virtualenv; a new directory called "venv" will
be created that holds all neccessary stuff.

- after initializing you can start/stop/restart 

~~~

cd makepw.app && ./makepw-web -init && ./makepw-web -start

~~~

### Cleanup

- if yopu installed an additional build-environment, delete it after installation. 

~~~
aptitude remove build-essential
~~~


### Migrate


- just copy the makepw.app - folder to the new destination
- run ./makepw-web -init on the new location to re-initialize 
- if changed your config.py, a backup might be found in web/config.py-$timestamp 


</div>
</div>
