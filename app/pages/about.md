[MakePW.com](http://makepw.com/) is a service that generates 
secure, random passwords, 24/7.
Default password-length is 16 characters and can be adjusted 
from 8 - 128 characters. There's also an option to get the 
password-list as plain ascii-text file or access an RESTful 
[API](/doc/#api) that creates JSON or YAML - output.


This webapp works with javascript disabled, except for 
navigation on smartphones; you
still are able adjust the value for password-length manually and
choose between 8 and 128 characters the following way: 
[https://makepw.com/32/](https://makepw.com/32/)

The sourcecode of this [python/flask](http://flask.pocoo.org/) - based 
WebApp is [released as open-source-software](https://bitbucket.org/lazy_dogtown/makepw.com/)  
to be used for individuals or within organizational infrastructure; please refer to the [installation](#install) - section
below to find needed manuals to setup and configure your own version. 

Our Privacy-Statement and Terms of Service are available [here](/info/#privacy).

