
<hr>
<a name="api"></a>
<div class="row" align="left">
<div  class="col-sm-2">
<img src="/static/barking-dog-med.png">
</div>
<div class="col-sm-10" >
    <br>
    <h2>API - Manual</h2>
</div>
</div>


<hr>

<div class="row" markdown="1" align="left">
<div  class="col-sm-2">

</div>
<div class="col-sm-10" markdown="1">


### API

An API is available below `/api/<format>/<pw-length>/<number-of-passwords>/`
and might be accessed via GET - requests

** available Formats **

- json -> return an JSON-object with an array of 10 passwords; mime-type: application/json
~~~
    {
        "passwords" : ["pw1", "pw2", ... "pwN"],
        "length"    : <pw-length>,
        "count"     : <number-of-passwords>,
    
    }

~~~

- yaml -> return result in yaml-format; mime-type: application/x-yaml [see](http://stackoverflow.com/questions/332129/yaml-mime-type)

~~~
--- # list of passwords
passwords: [pw1, pw2, ...., pwN]
length   : <pw-length>
count    : <number-of-passwords>
---
~~~

- plain -> return a list of passwords; mime-type: text/plain
~~~
pw1
pw2
....
pwN
~~~
- 

**PW-LENGTH**

- must be between (including) 8 and 128
- defaults to 16 / default_length via config 

**Number of Passwords**

- must be between (including) 1 and 128
- defaults to 8 / default_pw_count via config 

</div></div>
