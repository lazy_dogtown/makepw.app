

<hr>
<a name="theory"></a>
<div class="row" align="left">
<div  class="col-sm-2">
<img src="/static/barking-dog-med.png">
</div>
<div class="col-sm-10" >
    <br>
    <h2>Secure Passwords - musings</h2>
</div>
</div>


<hr>

<div class="row" markdown="1" align="left">
<div  class="col-sm-2">

</div>
<div class="col-sm-10" markdown="1">

#### What are secure passwords?


------------------

#### Is there any point in using 'strong' passwords?

- [src](http://security.stackexchange.com/questions/42134/is-there-any-point-in-using-strong-passwords)

**Question**

Let's say I'm setting up a new account on a website. Should I bother using a strong password? My reasoning is this:

- Password strength is a rough measure of how long it would take to brute-force my password from a hash. 
- Brute-forcing even a 'weak' password is difficult via the authentication endpoint for the website - 
it's too slow, and limitations on the number of incorrect password attempts are commonplace. 
- Brute-forcing even 'strong' passwords is becoming trivial when you have the password hash, 
and this situation will only get worse. 
- In the situation that password hashes are leaked from a website, passwords are reset anyway.

It seems to me that there's no difference between using the password `fredleyyyy` or `7p27mXSo4%TIMZonmAJIVaFvr5wW0%mV4KK1p6Gh` at the end of the day.

------------------------

**Answer** by Terry Chia

> Brute-forcing even 'strong' passwords is becoming trivial when you have the password hash, and this situation will only get worse. 

This is not true. A highly random password is near impossible to bruteforce given that the web application in 
question is using a strong hashing algorithm like bcrypt or pbkdf2. On the other hand, weak passwords are laughably 
easy to bruteforce even if a strong hashing algorithm is used.

So yes, there is merit to using a strong password.



------------------

#### How to generate secure passwords?

What makes these perfect and safe?

Every one is completely random (maximum entropy) without any pattern, and the cryptographically-strong pseudo random number generator we use guarantees that no similar strings will ever be produced again. 

 Also, because this page will only allow itself to be displayed over a snoop-proof and proxy-proof high-security SSL connection, and it is marked as having expired back in 1999, this page which was custom generated just now for you will not be cached or visible to anyone else. 

 Therefore, these password strings are just for you. No one else can ever see them or get them. You may safely take these strings as they are, or use chunks from several to build your own if you prefer, or do whatever you want with them. Each set displayed are totally, uniquely yours — forever. 

 The "Application Notes" section below discusses various aspects of using these random passwords for locking down wireless WEP and WPA networks, for use as VPN shared secrets, as well as for other purposes. 

 The "Techie Details" section at the end describes exactly how these super-strong maximum-entropy passwords are generated (to satisfy the uber-geek inside you).

#### How to securely store passwords?



#### Are there better ways than using passwords?


</div></div>
