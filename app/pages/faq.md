

<a name="install"></a>
<div class="row" align="left">
<div  class="col-sm-2">
<img src="/static/barking-dog-med.png">
</div>
<div class="col-sm-10" >
    <br>
    <h2>FAQ and Troubleshooting for makepw.app </h2>
</div>
</div>


<hr>

<div class="row" markdown="1" align="left">
<div  class="col-sm-2">

</div>
<div class="col-sm-10" markdown="1">


<a name="install"></a>


-----------------

### Docs: Installation & Configuration


- [Download](https://bitbucket.org/lazy_dogtown/makepw.com) sourcecode from bitbucket.org
- [Installation - Manual](/doc/#install), to be found in the sourcecode under pages/installation.md
- [Configuration - Manual](/doc/#config), to be found in the sourcecode under pages/configuration.md
- [API - Manual](/doc/#api), to be found in the sourcecode under pages/api.md
- [ChangeLog](/doc/#changelog), to be found in the sourcecode under pages/changelog.md
- [License](/info/#license), to be found in the sourcecode under License.txt


### FAQ / Troubleshooting 

#### i cannot start / Error: Already running on PID 

- during a recent stop, the pid wasnt cleaned; 
- please run ./makepw-web -restart


#### i cannot start / Connection in use: ('0.0.0.0', 9876)

- during a recent stop, the pid wasnt cleaned; 
- please run ./makepw-web -restart

- the error looks like this:

        > proudly serving secure passwords @ 0.0.0.09876
            
        > starting main engines
                
        nohup: redirecting stderr to stdout
        2013-09-10 15:28:30 [8008] [INFO] Starting gunicorn 
        2013-09-10 15:28:18 [7990] [ERROR] Connection in use: ('0.0.0.0', 9876)
        2013-09-10 15:28:18 [7990] [ERROR] Retrying in 1 second.
        2013-09-10 15:28:19 [7990] [ERROR] Connection in use: ('0.0.0.0', 9876)
        2013-09-10 15:28:19 [7990] [ERROR] Retrying in 1 second.
        2013-09-10 15:28:20 [7990] [ERROR] Connection in use: ('0.0.0.0', 9876)
        2013-09-10 15:28:20 [7990] [ERROR] Retrying in 1 second.
        2013-09-10 15:28:21 [7990] [ERROR] Connection in use: ('0.0.0.0', 9876)
        2013-09-10 15:28:21 [7990] [ERROR] Retrying in 1 second.


#### i have a lot of compile-errors while initializing

- please install `gcc` and `make` for initialising
- you can delete `gcc` and `make` afterwards


#### i cannot start the app / socket.error: [Errno 98] Address already in use

- another instance, listening on your choosen port is already active
- please run ./makepw-web -restart for restart, ./makepw-web -kill to end the application
- if this doesnt help: another process seems to use this port already; check with `netstat -pltn | grep PORT]`; 
when starting makepw.app you'll see a line like: `> proudly serving secure passwords @ 0.0.0.0:9876` that gives you a hint on which ip/port the webapp tries to bind




</div>
</div>
