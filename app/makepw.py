#
# makepw.com -> see online for an explanation
#               and documentation
#
# src: https://bitbucket.org/lazy_dogtown/makepw.com
#
# (c) copyright 2012,2013,2014 makepw.com
# 
# this software is free software and licensed unter the 
# terms of the GNU Affero General Public License; 
# see License.txt for more details
#
#


version = "0.8.10.rc4.52 - 2015-05-05"

import sys, hashlib, time, glob, os, markdown, string

from flask import Flask, render_template, Response, request, make_response, redirect, flash, jsonify
from werkzeug.contrib.fixers import ProxyFix
from functools import wraps
from datetime import datetime as dt
from time import strftime


sys.path.append("lib")
from saferproxyfix import SaferProxyFix



default_length = 16
default_pw_count = 8

app = Flask(__name__)
#app.wsgi_app = ProxyFix(app.wsgi_app)
app.wsgi_app = SaferProxyFix(app.wsgi_app)



# defaults - please dont change 

debug = False
site_name = "MakePW.com"
host="0.0.0.0"
port=9876
privatx = None 
about_name="About"
custom_links = []
custom_link_file = "custom.links"

try:
    app.config.from_object("config")
    debug = app.config["DEBUG"]
except:
    pass 
    

try:
    site_name = app.config["SITE_NAME"]
except:
    pass 

try:
    privatx = app.config["PRIVATE"]
    if privatx != "no":
        privatx = None
        try:
            l = open(custom_link_file).readlines()
            for el in l:
                er = el.split("::")
                if len(er) > 1:
                    en = er[0].strip()
                    et = "::".join(er[1:])
                    custom_links.append((en, et))
            print("> custom_links( %s ) generated" % len(custom_links))
            
        except:
            print("[-] custom_links_file [ %s ] is not parseable" % custom_link_file)
except:
    privatx = None
    try:
        l = open(custom_link_file).readlines()
        for el in l:
            er = el.split("::")
            if len(er) > 1:
                en = er[0].strip()
                et = "::".join(er[1:])
                custom_links.append((en, et))
        print("> custom_links( %s ) generated" % len(custom_links))
        
    except:
        print("[-] custom_links_file [ %s ] is not parseable" % custom_link_file)

try:
    about_name = app.config["ABOUT_NAME"]
except:
    pass 


try:
    debuge = os.environ['MKPW_APP_DEBUG']
    if debuge == "yes":
        debug = True 

except:
    pass 


if debug == True:
    print("> Debug: %s" % debug)
     
else:
    import logging
    log = logging.getLogger('werkzeug')
    log.setLevel(logging.ERROR)

            



charset='utf-8'


app.config["DEBUG"] = debug 


app.jinja_env.globals['version'] = version
app.jinja_env.globals['site_name'] = site_name
app.jinja_env.globals['privatx'] = privatx
app.jinja_env.globals['about_name'] = about_name
app.jinja_env.globals['custom_links'] = custom_links


    

@app.route('/',  methods=['GET'])
@app.route('/<path:xtr>/',  methods=['GET'])
@app.route('/<path:xtr>/<path:act>/',  methods=['GET'])
def main_index(xtr=0, act = 0):

    # must be
    try:
        xtr = int(xtr)
    except:
        print "unknow XTR: %s" % xtr
        return(redirect("/"))
    
    if act == "reload":
        return(redirect("/%s/" % xtr))

    
    pw, xtc = make_all(xtr)
    
    return(render_template("passwords.html", passwords = pw, length = xtc ))

@app.route('/plain/',  methods=['GET'])
@app.route('/plain/<path:xtr>/',  methods=['GET'])
def main_plain(xtr=0):

    # must be
    try:
        xtr = int(xtr)
    except:
        return(redirect("/plain/"))

    pw, xtc = make_all(xtr)
    
    out = """#
# makepw.com 
# 
# pw-length: %s
#
# https://makepw.com/<length>
# https://makepw.com/info 
# 
%s
""" % (xtc, "\n".join(pw))

    return Response(out, mimetype='text/plain')


@app.route('/info/',  methods=['GET'])
def main_info():
    
    cd = render_content("documentation")
    return render_template('content.html',  
        cd = cd, length = 16
        
        )

@app.route('/about/',  methods=['GET'])
def r_about():
    return(redirect("/info/"))

@app.route('/doc/',  methods=['GET'])
def main_docs():
    
    cd = render_content('docu_header') +  render_content("installation") + render_content("configuration")  + render_content("api") + render_content("theory") + render_content("changelog") 
    
    return render_template('content.html',  
        cd = cd, length = 16,
        
        )

@app.route('/api/<path:aformat>/',  methods=['GET'])
@app.route('/api/<path:aformat>/<path:pwl>/',  methods=['GET'])
@app.route('/api/<path:aformat>/<path:pwl>/<path:cnt>/',  methods=['GET'])
def main_api(aformat=0,pwl=default_length, cnt=default_pw_count):
    if aformat == 0:
        return(redirect("/doc/#api"))

    try:
        int(pwl)
    except:
        pwl = default_length

    try:
        int(cnt)
    except:
        cnt = default_pw_count

    

    pwall = make_all(pwl, cnt)
    if aformat == "json":
        pwlist = {}
        pwlist["passwords"] = pwall[0] 
        pwlist["length"] = pwl
        pwlist["count"] = cnt
        return jsonify(pwlist)

    elif aformat == "plain":
        out = "\n".join(pwall[0])
        return Response(out, mimetype='text/plain')
    elif aformat == "yaml":
        out = """--- list of passwords
passwords: [ %s ] 
length   : %s
count    : %s
---""" % (",".join(pwall[0]), pwl, cnt)
        return Response(out, mimetype='application/x-yaml')
    else:
        return(redirect("/api-not-supported-format-%s" % aformat))
    application/x-yaml
    return(redirect("/doc/#api"))


######################################################################


def make_all(xtr, pwCC = default_pw_count):
    
    try:
        xtr = int(xtr)
    except:
        xtr = default_length
    if   xtr < 8  or xtr > 128:
        xtr = default_length

    try:
        pwCC = int(pwCC)
    except:
        pwCC = default_pw_count
    if   pwCC < 1  or pwCC > 128:
        pwCC = default_pw_count
    

    pw_count = pwCC

    
    pwc = 0
    pw = []

    while pwc < pw_count:
        pwc += 1
        pwx = make_pw(xtr)
        pw.append(pwx)
    
    return(pw, xtr)

    
def make_pw(l=32):
    #
    # see http://security.stackexchange.com/a/41503/27702
    # credits: Brendan Long
    #
    from random import SystemRandom
    from string import ascii_letters as str_ascii
    from string import digits as str_digits
    
    rng = SystemRandom()
    

    allchars = str_ascii + str_digits

    
    try:
        passwordLength = int(l)
    except:
        #user didn't specify a length.  that's ok, just use 8
        passwordLength = 32

    # not less 
    if passwordLength < 8:
        passwordLength = 32
    elif passwordLength > 128:
        passwordLength = 32

    pw = "".join([rng.choice(allchars) for i in range(passwordLength)])
    
    return(pw)
    

def render_content(in_put):
    """


    
    """
    basedir="pages"
    fp = "%s/%s.md" % (basedir, in_put)
    is_file = glob.glob(fp)
    if not is_file:
        print "cannot access %s" % fp
        resp = """
        <div align="center">
        <h1>404 - DONT PANIC</h1>
        the file you requested was not found
        <img src="/static/images/marvin4.jpg" width="70%">
        </div>
        """
        return(resp)
    else:
        
        rout = mdown("%s/%s.md" %  (basedir, in_put))
        return(rout)
    
    return("marvin is VERY tired")


def mdown(in_file):
    if not os.path.isfile(in_file):
        print "infile: %s" % in_file
        return(1000)
    
    
    
    
    # 'sane_lists', 'meta', 'extra', 'tables', 'fenced_code', 'codehilite'])
    ext = ['meta', 'extra', 'fenced_code', 'tables', 'codehilite', 'toc', 'attr_list']
    #md = markdown2.Markdown(extras=ext)
    #md = markdown2.Markdown(extras=ext)
    fxi = ""
    fx = "".join(open(in_file, "r").readlines()).decode(charset)
    
    #print fx
    fxout = markdown.markdown(fx, extensions=ext).replace("<img alt=", "<img id=\"blog_img\" alt=")
    #fxout = md.convert(fx).replace("<li>\n<p>", "<li>").replace("</p>\n</li>", "</li>").replace("<img alt=", "<img id=\"blog_img\" alt=")
    return(fxout)
    #return(markdown.markdown(fxi))

g_about = render_content("about")
app.jinja_env.globals['about'] = g_about


if __name__ == '__main__':


    try:
        rhost = os.environ['MKPW_APP_HOST']
    except:
        rhost="0.0.0.0"

    try:
        rport = int(os.environ['MKPW_APP_PORT'])
    except:
        rport = 9876

    # generating and ensuring indizes
    print "> starting makepw.app @ %s : %s" % (rhost, rport)

    

    app.run(host=rhost,port=rport)






