#
# manage.py for makepw.com
#
# 


version = "0.1.12 - 2013-09-10"

from flask.ext.script import Manager
from makepw import app, make_pw
from shutil import copyfile
import os, time 

manager = Manager(app)

@manager.command
def generate_pw(l=32):
    pw_out = make_pw(l)
    print pw_out
    
@manager.command
def create_config():
    print("[i] generating config_file")
        
    cfg = "config.py"
    if os.path.isfile(cfg):
        dst = "%s-%s" % (cfg, int(time.time()))
        copyfile(cfg, dst)
        print("  > found %s -> copied to %s" % (cfg, dst))
    f = open(cfg, "w")
    f.write("""
SITE_NAME="MakePW.com"
DEBUG = False
SECRET_KEY = "%s"
ABOUT_NAME = "Info"

""" % make_pw(128))
    f.close()
    print("  > new config-file created -> %s" % cfg)
    print("    you might edit it manually")

    cl = "custom.links"
    if not os.path.isfile(cl):
        print("  > creating new custom.links - file -> %s " % (cl))
        f = open(cl, "w")
        f.write("""
Home            :: /
Info            :: /info/
Documentation   :: /doc/
Google          :: https://www.google.com/

""" % make_pw(128))
        f.close()
        print("  > new custom.links file created -> %s" % cl)
        print("    you might edit it manually")
    


if __name__ == "__main__":
    manager.run()
