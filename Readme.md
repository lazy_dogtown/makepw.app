

Readme for Sourcecode of MakePW.com - HTTP-based Password-Generator


- [About](http://makepw.com/about/)
- [Installation Manual](http://makepw.com/doc/#install)
- [Configuration Manual](http://makepw.com/doc/#config)
- [API Manual](http://makepw.com/doc/#api)
- [Changelog](http://makepw.com/doc/#changelog)


---------------


[MakePW.com](http://makepw.com/) is a free service that generates random passwords with
default-length of 16 characters; password-length can be adjusted 
from 8 - 128 characters, and there's also an option to get the 
password-list as plain ascii-text file. An API is planned. 


There's also the possibility to run this [python/flask](http://flask.pocoo.org/) - based 
WebApp locally or within your organization; please refer to the [installation-manual](http://makepw.com/doc/#install) - section
below to find needed manuals to setup and configure your own version. 
